<?php
defined('BASEPATH') OR exit('No direct script access allowed'); 
/* front site */
$route['default_controller'] = 'users/login';
$route['login'] = 'users/login';
$route['logout'] = 'users/logout';
$route['reset-password'] = 'users/reset_password_frontend';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

$route['privacy_policy'] = 'pages/privacy_policy';

/* admin routing */
$route[ADMIN . '/dashboard'] = "users/".ADMIN."/users/dashboard";
$route[ADMIN.'/orders/create-order/(:any)'] = "orders/".ADMIN."/orders/add_to_cart/$1";
$route[ADMIN.'/orders/order-details?(:any)'] = "orders/".ADMIN."/orders/order_details/$1";
$route[ADMIN.'/orders/invoice/(:any)'] = "orders/".ADMIN."/orders/download_invoice/$1";

$route[ADMIN.'/states/product-limit-create'] = "states/".ADMIN."/states/limit_create";
$route[ADMIN.'/states/product-limit-edit/(:any)'] = "states/".ADMIN."/states/limit_edit/$1";
$route[ADMIN.'/states/product-limit-list'] = "states/".ADMIN."/states/limit_list";
$route[ADMIN.'/users/retailer-shops'] = "users/".ADMIN."/users/retailer_shops";

$route[ADMIN . '/(:any)/(:any)'] = "$1/". ADMIN . "/$1/$2";
$route[ADMIN . '/(:any)'] = "$1/". ADMIN . "/$1";
$route[ADMIN . '/(:any)/(:any)/(:any)'] = "$1/". ADMIN . "/$1/$2/$3";

