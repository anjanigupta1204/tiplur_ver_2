<?php

class Orders extends MX_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('orders_model');
        $this->load->library('auth');
        $this->load->library('form_validation');

        $this->set_data = array('theme' => 'admin'); //define theme name 
        if (!$this->auth->_is_logged_in()) {
            redirect(base_url());
        }
    }

    function index() {
        is_admin();
        //pagination 
        $this->load->library('pagination');
        $per_page = isset($_GET['per_page']) ? $_GET['per_page'] : false;
        unset($_GET['per_page']);
        $query_string = http_build_query($_GET);

        $site_url = !$query_string ? base_url(SITE_AREA . '/orders') : base_url(SITE_AREA . '/orders?' . $query_string);
        if ($per_page) {
            $_GET['per_page'] = $per_page;
        }
        $num_rows = $this->orders_model->orders_listing($this->filter());
        $config = pagination_formatting();
        $limit = 20;
        $config['base_url'] = $site_url;
        $config['total_rows'] = $num_rows;
        $config['per_page'] = $limit;
        $config['num_links'] = 5;
        $config['use_page_numbers'] = TRUE;
        $config['page_query_string'] = TRUE;
        $this->pagination->initialize($config);

        $offset = isset($_GET['per_page']) ? ($_GET['per_page'] - 1) * $limit : null;
        $data['orders'] = $this->orders_model->orders_listing($this->filter(), $limit, $offset, false);
        //end here 
        //prd($data['orders']);

        $data['css'] = array('/theme/admin/css/bootstrap-datetimepicker.css');
        $data['js'] = array('/theme/admin/js/moment.js', '/theme/admin/js/bootstrap-datetimepicker.js', '/assets/js/orders.js');
        $this->theme($data);
    }

    function filter() {
        return $this->input->get(array('retailer', 'status', 'from_dt', 'to_dt'));
    }

    function view_order() {

        $filter = $this->input->get(array('id', 'user_id', 'type'));

        $ids = array();

        if (isset($filter['id'])) {
            array_push($ids, $filter['id']);
        } else {
            if (isset($filter['user_id']) && isset($filter['type'])) {

                if ($filter['type'] == 'retailer' || $filter['type'] == 'user') {
                    $ids = $this->orders_model->get_user_orders($filter['user_id'], $filter['type']);
                }
            }
        }

        $data['ids'] = $ids;
        $data['js'] = array('/theme/admin/js/moment.js', '/theme/admin/js/bootstrap-datetimepicker.js', '/assets/js/orders.js');
        $data['status_history'] = $this->orders_model->getOrderHistory($filter['id']);
        $this->theme($data);

        //prd($order_details);
    }
	
	function abandoned_cart() {

		$this->load->library('pagination');
        $per_page = isset($_GET['per_page']) ? $_GET['per_page'] : false;
        unset($_GET['per_page']);
        $query_string = http_build_query($_GET);

        $site_url = !$query_string ? base_url(SITE_AREA . '/orders/abandoned_cart') : base_url(SITE_AREA . '/orders/abandoned_cart?' . $query_string);
        if ($per_page) {
            $_GET['per_page'] = $per_page;
        }
        $num_rows = $this->db->get('abandoned_cart_detail')->num_rows();
        $config = pagination_formatting();
        $limit = 20;
        $config['base_url'] = $site_url;
        $config['total_rows'] = $num_rows;
        $config['per_page'] = $limit;
        $config['num_links'] = 5;
        $config['use_page_numbers'] = TRUE;
        $config['page_query_string'] = TRUE;
        $this->pagination->initialize($config);

        $offset = isset($_GET['per_page']) ? ($_GET['per_page'] - 1) * $limit : null;
        $data['result'] = $this->db->limit($limit, $offset)->order_by('created_on','desc')->get('abandoned_cart_detail')->result();

        $this->theme($data);

        //prd($order_details);
    }

    public function export_orders() {

        $filter = isset($_GET['filter']) ? $this->filter() : false;

        $reports = $this->orders_model->orders_listing($filter, false, false, false);
       // echo "<pre>"; print_r($reports);die;

        /* Start Logic for excel */
        $exceldata = array();
        $table = '<table><tr><th>Sr No.</th><th>Order No</th><th>Customer</th><th>Retailer</th><th>Shop</th><th>Delivery Address</th><th>Product Name</th><th>Quantity</th><th>Weight</th><th>Price</th><th>Total Amount</th><th>Payment Mode</th><th>Order Date</th><th>Status</th><th>Acceptance Date & Time</th><th>Ongoing Date & Time</th><th>Delivered Date & Time</th></tr>';
        
        $total_amount = 0;
		$order_id = null;
		$i = 1;
        foreach ($reports as $record) {
			//print_r($reports);
			$status_details = $this->db->select('status,created_date')->where('order_number',$record->order_id)->where_in('status',array('1','4','5'))->get('trans_order_status')->result_array();
			$status_arr   = !empty($status_details) ? array_column($status_details,'created_date','status'):'';
			
			$Order_Details = get_order_details($record->order_id);
            $products = explode('<@>',$Order_Details->product_id);
			$quantity = explode('<@>',$Order_Details->quantity);
			$price    = explode('<@>', $Order_Details->price);
            $weight   = explode('<@>', $Order_Details->weight);
			$j = 0;
            foreach($products as $p):
				$table .= '<tr>';
				$table .= ($order_id != $record->order_id) ? '<td>' . $i . '</td>' : '<td></td>';
				$table .= ($order_id != $record->order_id) ? '<td>' . $record->order_id . '</td>' : '<td></td>';
				$table .= ($order_id != $record->order_id) ? '<td>' . $record->user:'</td>'. '<td></td>';
				$table .= ($order_id != $record->order_id) ? '<td>' . $record->retailer:'</td>'. '<td></td>';
				$table .= ($order_id != $record->order_id) ? '<td>' . store_detail_by_retailer_id($record->retailer_id)->store_name:'</td>'. '<td></td>';
				$table .= ($order_id != $record->order_id) ? '<td>' . $record->address . ',' . $record->city . ',' . $record->state . ',' . $record->country :'</td>'. '<td></td>';
				$table .= '<td>' . $p . '</td>';
				$table .= '<td>' . $quantity[$j].'</td>';
				$table .= '<td>' . $weight[$j].'</td>';
				$table .= '<td>' . $price[$j].'</td>';
				$table .= ($order_id != $record->order_id) ? '<td>' . $Order_Details->total:'</td>'. '<td></td>';
				$table .= ($order_id != $record->order_id) ? '<td>' . $record->title:'</td>'. '<td></td>';
				$table .= ($order_id != $record->order_id) ? '<td>' . date('d M Y h:i A', strtotime($record->order_date)) :'</td>'. '<td></td>';
				$table .= ($order_id != $record->order_id) ? '<td>' . print_order_status($record->status) :'</td>'. '<td></td>';
			    $table .= ($order_id != $record->order_id  && !empty($status_arr[1])) ? '<td>' . date('d M Y h:i A', strtotime($status_arr[1])) :'</td>'. '<td></td>';
				$table .= ($order_id != $record->order_id && !empty($status_arr[4])) ? '<td>' . date('d M Y h:i A', strtotime($status_arr[4])) :'</td>'. '<td></td>';
			    $table .= ($order_id != $record->order_id && !empty($status_arr[5])) ? '<td>' . date('d M Y h:i A', strtotime($status_arr[5])) :'</td>'. '<td></td>';
                $table .= '</tr>';
				$j++;
				$order_id = $record->order_id;
			endforeach;
			$i++;
            $table .= '<tr></tr>';
        }
        $table .= '</table>';
        $date = date('d-m-Y');

        header("Content-Type: application/vnd.ms-excel");
        header("Content-disposition: attachment; filename=Tipular Order Details(" . $date . ").xls");
        echo $table;
        die();
        /* end Logic for excel */
    }
    
    function decline_order() {

        if (empty($_POST)) {
            show_404();
        }

        $id = $this->input->post('order_id');
        $reason = $this->input->post('reason');

        $order_details = get_order_details($id);
        //prd($order_details);
        $data = array('status' => '2', 'modification_date' => date('Y-m-d H:i:s'));

        $result = true;
        $updated = $this->orders_model->update_order($data, $id, $reason);
        //echo $updated;exit;
        if ($updated == false) {
            $result = false;
        }

        if ($result) {

            $order_details = get_order_details($id);
            
            $notify_data = array(
                'order_id' => 'Order Declined',
                'notified_to' => $order_details->user_id,
                'requested_by' => $this->session->userdata('id'),
                'type' => ORDER_DECLINED_NOTIFICATION,
                'title' => 'Order Declined',
                'message' => 'Your order is declined by admin',
                'reason' => $reason,
            );

            $notify = new Push_notification();
            $notify->send_notification($notify_data);

            //$email = $order_details->user_email;

            //$this->send_welcome_mail($email); //send mail

            set_flash_message($type = 'success', $message = 'Order declined successfully.');
            redirect('admin/orders');
        } else {
            set_flash_message($type = 'error', $message = 'something wrong.');
            redirect('admin/orders');
        }
    }


    function chnage_order_status() {

        if (empty($_POST)) {
            show_404();
        }

        $id = $this->input->post('order_id');
        $reason = $this->input->post('reason');
		$status = $this->input->post('status');
		$status_detail = order_status($status);
		$status_name = ($status_detail) ? $status_detail->title :'';

        $data = array('status' => $status, 'modification_date' => date('Y-m-d H:i:s'));

        $result = true;
        $updated = $this->orders_model->update_order($data, $id, $reason);
        //echo $updated;exit;
        if ($updated == false) {
            $result = false;
        }

        if ($result) {

            $order_details = get_order_details($id);
            if($status == '2'):
				$notify_data = array(
					'order_id' => 'Order Declined',
					'notified_to' => $order_details->user_id,
					'requested_by' => $this->session->userdata('id'),
					'type' => ORDER_DECLINED_NOTIFICATION,
					'title' => 'Order Declined',
					'message' => 'Your order is declined by admin',
					'reason' => $reason,
				);

				$notify = new Push_notification();
				$notify->send_notification($notify_data);
		    endif;

            //$email = $order_details->user_email;

            //$this->send_welcome_mail($email); //send mail

            set_flash_message($type = 'success', $message = 'Order Status Updated successfully.');
            redirect('admin/orders');
        } else {
            set_flash_message($type = 'error', $message = 'something wrong.');
            redirect('admin/orders');
        }
    }

}
