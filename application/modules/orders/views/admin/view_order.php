<div id="page-wrapper" class="gray-bg dashbard-1">
    <?php //breadcrumbs(array('admin/orders' => 'Order List')); ?>
    <div class="row border-bottom">

    </div>
    <!--<div class="row  border-bottom headWidth dashboard-header">
        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                <h2 class="salesHead">Order List</h2>
                <select class="tableDrop headingSelect input-sm form-control input-s-sm inline">
                    <option value="0">Action</option>
                    <option value="1">Option 2</option>
                    <option value="2">Option 3</option>
                    <option value="3">Option 4</option>
                </select>
            </div>
        </div>
    </div>-->
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">                
                <div class="ibox float-e-margins">
                    <div class="ibox-title borderNone">
                        <h2>Order Details </h2>
                    </div>
                    <div class="ibox-content borderNone">
                        <div class="table-responsive">
                            <table class="table table-striped">
                                <thead>
                                    <tr>                                        
                                        <th>Order No</th>                                        
                                        <th><?php echo (isset($_GET['type']) && $_GET['type'] == 'user') ? 'Retailer' : 'Order By'; ?></th>
                                        <th>Order Items</th>
                                        <th>Order Total</th>
                                        <th>Order On</th>
										<th>Instruction</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>                                
                                    <?php
                                    if (!empty($ids)):
                                        foreach ($ids as $id):
                                            $order = get_order_details($id);
                                            ?>
                                            <tr>
                                                <td><?php echo $order->order_id; ?></td>
                                                <td><?php echo (isset($_GET['type']) && $_GET['type'] == 'user') ? $order->retailer : $order->user; ?></td>
                                                <td><ul><?php
                                                        $products = explode('<@>', $order->product_id);
                                                        $quantity = explode('<@>', $order->quantity);
                                                        $price = explode('<@>', $order->price);
                                                        $weight = explode('<@>', $order->weight);
                                                        $i = 0;
                                                        foreach ($products as $product) {
                                                            ?>

                                                            <li><?php echo $product . "(".$weight[$i].")  ".intval($price[$i])." <strong>x</strong> " . $quantity[$i] . ' = Rs.' . ($price[$i] * $quantity[$i]); ?></li>

                                                            <?php
                                                            $i++;
                                                        }
                                                        ?></ul></td>
                                                <td><?php echo 'Rs.' . $order->total; ?></td>                                                                                                                                 
                                                <td><?php echo site_date_time($order->creation_date); ?></td> 
												<td><?php echo $order->instruction ? $order->instruction:'No Instruction'; ?></td> 
												<td><?php echo print_order_status($order->status); ?></td>
                                                <td><?php /*if ($order->status == 7) { ?><button type="button" class="btn btn-default order-decline" data-id="<?php echo $order->order_id; ?>">Decline Order</button><?php }*/ ?>
												<button type="button" class="btn btn-default order-decline" data-id="<?php echo $order->order_id; ?>">Change Order Status</button></td>
                                            </tr>
                                            <?php
                                        endforeach;
                                    else:
                                        echo "<tr><td colspan='12'>No Result Found</td></tr>";
                                    endif;
                                    ?>
                                </tbody>
                            </table>
                        </div>
						<div class="col-md-5 ordr-sta">
							<h3 >ORDER STATUS DETAILS</h3>

							<ul class="list-group">
							<?php if(!empty($status_history)): 
							          foreach($status_history as $history):
							?>
								<li class="list-group-item dis-inblock">
									<span class="prefix"><?php echo print_order_status($history->status); ?> :</span>
									<span style="margin-left:20px;"> 
									<?php  echo date('d M Y H:i A',strtotime($history->created_date)); ?> 
									<?php echo ($history->comments) ? '[ '.$history->comments.' ]':'' ?>
									<?php //echo ($history->delivered_at) ? '[ Delivered Time-'.$history->delivered_at.' ]':'' ?>
									</span>
								</li>
							<?php endforeach; endif; ?>	
							</ul>
				        </div>
						<div class="clearfix"></div>
					</div>
				 </div>
				   
            </div>
        </div>
        <div class="row">
            <div class="col-sm-5">
                <div class="dataTables_info tableData" id="editable_info" role="status" aria-live="polite"></div>
            </div>
        </div>
    </div>
</div>


<div id="declineOrderModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <form action="<?php echo base_url('admin/orders/chnage_order_status'); ?>" method="post">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Change Order Status</h4>
                </div>
                <div class="modal-body">   
                    <div class="form-group formWidht">
                         <label>Status</label>
                         <select class="form-control" name="status" required>
							<option value="">Please select</option>
							<?php
							$status = order_status();
							foreach ($status as $row) {
								?>
								<option value="<?php echo $row->id; ?>" <?php if (isset($_GET['status']) && $_GET['status'] == $row->id) { ?>selected<?php } ?>><?php echo $row->title; ?></option>
							<?php }
							?>
                         </select>
                    </div>				
                    <div class="form-group formWidht">
                        <label>Status Reason <span style="color: red;">*</span></label>
                        <input type="text" placeholder="" required name="reason" value="<?php echo set_value('reason', ''); ?>" id="exampleInputEmail2" class="form-control formWidht">
                    </div>
					
                    <input type="hidden" id="order_id" name="order_id" value=""/>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success">Submit</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>				
                </div>
            </div>
        </form>
    </div>
</div>


<?php /*
<!-- Modal -->
<div id="declineOrderModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <form action="<?php echo base_url('admin/orders/decline_order'); ?>" method="post">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Decline Order</h4>
                </div>
                <div class="modal-body">                    
                    <div class="form-group formWidht">
                        <label>Decline Reason <span style="color: red;">*</span></label>
                        <input type="text" placeholder="" required name="reason" value="<?php echo set_value('reason', ''); ?>" id="exampleInputEmail2" class="form-control formWidht">
                    </div>
                    <input type="hidden" id="order_id" name="order_id" value=""/>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success">Submit</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>				
                </div>
            </div>
        </form>
    </div>
</div>
*/ ?>


