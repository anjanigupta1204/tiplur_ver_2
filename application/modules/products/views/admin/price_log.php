<div id="page-wrapper" class="gray-bg dashbard-1">
    <?php breadcrumbs(array('admin/products/price_log' => 'Manage Retailer Price Log')); ?>
    <div class="row border-bottom">
    </div>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12 " >
                <?php print_flash_message(); ?>
                <div class="ibox float-e-margins">

                    <div class="ibox-title" style="margin-bottom: 20px;">
                        <?php echo form_open($this->uri->uri_string(), array('method' => 'get')); ?>
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group formWidht">
                                    <label>Retailer</label>
                                    <select id="retailer_id" name="retailer_id" class="form-control">
                                        <option value="">Please select</option>
                                        <?php
                                        $retailer = retailers();
                                        if (!empty($retailer)):
                                            foreach ($retailer as $val):
                                                ?>
                                                <option value="<?php echo $val->id; ?>" <?php echo ($val->id == @$_GET['retailer_id']) ? 'selected' : ''; ?>  >
												<?php if($val->status =='9') 
											      { echo ucfirst($val->display_name).' (Mapped)';
												  }else{ echo ucfirst($val->display_name);  } 
											    ?>
												</option>
                                                <?php
                                            endforeach;
                                        endif;
                                        ?>
                                    </select>
                                </div>                               
                            </div>

                            <div class="col-md-3">
                                <div class="form-group formWidht">
                                    <label>Product</label>
                                    <?php
                                    $product_id = isset($_GET['product_id']) ? $_GET['product_id'] : '';
                                    products_dropdown($product_id, 'form-control');
                                    ?>
                                </div>
                            </div>

                            <div class="col-md-2 filter-div">
                                <input type="submit" name="filter" id="" class="btn btn-success" value="Filter">
                                <?php if (isset($_GET['filter'])): ?>
                                    <a href="<?php echo base_url('admin/products/price_log'); ?>" class="btn btn-warning">Reset</a>
                                <?php endif; ?>
                            </div>

                        </div>
                        <?php echo form_close(); ?>
                    </div>

                    <div class="ibox-title">
                        <h2>View Retailers Price Logs </h2>
                    </div>
                    <div class="table-responsive">
                        <?php echo form_open($this->uri->uri_string(), array('class' => "form-horizontal", 'id' => 'product-listing', 'method' => 'get')); ?>
                        <div class="ibox-content borderNone">

                            <table class="table table-striped">
                                <thead>
                                    <tr>                                        
                                        <th>Retailer Name</th>
                                        <th>Product Name</th>                                        
                                        <th>Updated Price</th>
                                        <th>Updated On</th>                                        
                                    </tr>
                                </thead>
                                <tbody>                                   
                                    <?php
                                    if ($result):
                                        foreach ($result as $val):
                                            ?>
                                            <tr>
                                                <td><?php echo $val->display_name; ?></td>
                                                <td><?php echo $val->title; ?></td>
                                                <td><?php echo 'Rs.'.$val->price; ?></td>
                                                <td><?php echo $val->updated_at; ?></td>                                                
                                            </tr> 
                                            <?php
                                        endforeach;
                                    else:
                                        ?>
                                        <tr><td colspan="12">Result not found.</td></tr>
                                    <?php endif; ?>
                                </tbody>
                            </table>
                            </form>                            
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-5">
                <div class="dataTables_info tableData" id="editable_info" role="status" aria-live="polite"></div>
            </div>
            <?php echo $this->pagination->create_links(); ?>
        </div>
    </div>
</div>