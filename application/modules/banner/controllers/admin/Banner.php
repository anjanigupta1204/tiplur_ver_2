<?php
class Banner extends MX_Controller{

    function __construct() {
        parent::__construct();
		$this->load->model('banner_model');
		$this->load->library('auth');
		$this->load->library('form_validation');
		
		$this->set_data = array('theme'=>'admin'); //define theme name 
		if(!$this->auth->_is_logged_in())
		{
		  redirect(base_url());
		}
		is_admin();
	} 
	
	public function index()
	{
		if (!empty($_GET['multi_activate'])) {
            $this->banner_image_status('activate');
        }

        if (!empty($_GET['multi_deactivate'])) {
            $this->banner_image_status('de-activate');
        }
		
		$data['js'] =  array('/assets/js/banner.js');
		$data['result'] = $this->banner_model->get_banner_data();
		$data['temp'] = '';
		$this->theme($data);
	}
	
	
	function create() 
	{
		$data['js'] =  array('/assets/js/banner.js');  
		if (isset($_POST['save'])) 
		{
            if (empty($_FILES['image']['name'])) 
			{ 
			    set_flash_message($type = 'error', $message = 'Please select the banner image.');
			} else 
			{   
		        if ($result=$this->save_banner('insert')) {
                    set_flash_message($type = 'success', $message = 'Banner Image Added Successfully.');
                    redirect('admin/banner');
                }
            }
        }
        $data['temp'] = '';
        $this->theme($data);
    }
	
	/* script for save banner images */
    private function save_banner($type = false, $id = false) 
	{
        $b_data['status'] = '1';
		$b_data['type'] = $this->input->post('type');
		$b_data['created_on'] = db_date_time();
        $b_data['image'] = $this->upload_image();
		
        if ($type == 'insert') {
            $id  = $this->db->insert('banner_images',$b_data); 
            if ($id) {
                $return = $id;
            }
        } 
        return $return;
    }
	
	public function upload_image() {
        $this->load->library('upload');
        $upload_path = FCPATH . "assets/images/banner_images/";
        $input_field_name = 'image';
        $config = array(
            'upload_path' => $upload_path,
            'allowed_types' => 'gif|jpg|png|jpeg',
            'file_name' => time() . $_FILES[$input_field_name]['name'],
        );
        if (is_uploaded_file($_FILES[$input_field_name]['tmp_name'])) {
            $this->upload->initialize($config);
            if (!$this->upload->do_upload($input_field_name)) {
                $this->form_validation->set_message($input_field_name, $this->upload->display_errors());
                return false;
            } else {
                $uploaded = $this->upload->data();
                return $uploaded['file_name'];
            }
        } 
    }
	
	function banner_image_status($status) {

        $checked = $this->input->get('banner_id');
        if (is_array($checked) && count($checked)) {
           
            $result = true;
            foreach ($checked as $pid) {

                if ($status == 'activate') {
                    $data = array('status' => '1');
                } else {
                    $data = array('status' => '0');
                }

                $updated = $this->banner_model->update_banner($data, $pid);
                
                if ($updated == false) {
                    $result = false;
                }
            }
            if ($result) 
			{
                set_flash_message($type = 'success', $message = count($checked) . ' row updated successfully.');
                redirect('admin/banner');
            } else {
                set_flash_message($type = 'error', $message = 'something wrong.');
                redirect('admin/banner');
            }
        } else {
            set_flash_message($type = 'error', $message = 'Please select images.');
            redirect('admin/banner');
        }
    }
	
	function delete_banner_img() 
	{  
	   $id = $this->input->post('file_id');
       $file_name = $this->input->post('file_name');	   
	   if(!empty($id))
	   {
		 $res =   $this->db->where('id',$id)->delete('banner_images');
		 unlink(FCPATH."/assets/images/banner_images/".$file_name);
		 if($res)
		 {
			    set_flash_message($type = 'success', $message = 'Delete successfully.');
				echo '1';
		 }
	   }
	  
	}

	
}
