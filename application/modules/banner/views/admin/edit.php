<div id="page-wrapper" class="gray-bg dashbard-1">
    <?php breadcrumbs(array('admin/banner/upload-banner-images'=>'Add Banner Images'));  ?>
    <div class="row border-bottom"></div>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
		<?php print_flash_message(); ?>
                        <div class="col-lg-12">
               <?php echo form_open_multipart($this->uri->uri_string(), 'class="form-horizontal"', 'autocomplete="off"'); ?>
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h2 style="display: inline-block;">Upload Banner Images</h2>                               
                    </div>
                    <div class="ibox-content contentBorder">
                        <div class="row">
                            <div class="col-lg-6 col-md-4 col-sm-4 ">
                                <div class="form-group formWidht">
                                    <label>Order No.<span style="color: red">*</span></label>
                                    <input type="number" required1 name="order" min="1" max="20" value="<?php echo set_value('order',$result->order_no); ?>" id=""  class="form-control formWidht">
									<input type="hidden"  name="old_order" value="<?php echo set_value('order',$result->order_no); ?>" id=""  class="form-control formWidht">
                                </div>
                                <span class='error vlError'><?php echo form_error('order'); ?></span>
                            </div>
							<div class="col-lg-3 col-md-4 col-sm-4 AddProdctInputCont">
							<div class="form-group formWidht">

									<?php $select = !empty($result->type)?trim($result->type):$_POST['type']; ?>
									<label>Type <span style="color: red">*</span></label>
									<select class="form-control m-b addContDrop" required name="type">
									<option value=" ">Select</option>
									<option value="mobile" <?php echo ($select =='mobile')?'selected':''; ?> >APP</option>
									<option value="website" <?php echo ($select =='website')?'selected':''; ?> >Website</option>
                                    </select>
							</div>
							<span class='error vlError'><?php echo form_error('type'); ?></span>
                           
                            </div>
                        <div class="col-lg-3 col-md-4 col-sm-4 AddProdctInputCont">
                                        <div class="form-group formWidht ">
                                            <label>Status   <span>*</span></label>
                                            <select name="status"  required <?php echo (!empty($this->uri->segment(4))) ? '' : 'readonly'; ?> class="form-control m-b addContDrop product_category ">
                                                <?php
                                                $check = (isset($result->status) && $result->status == 1) ? $result->status : 2;
                                                $status = status();
                                                foreach ($status as $k => $val):
                                                    ?>
                                                    <option readonly value="<?php echo $k; ?>" <?php echo ($k == @$check) ? 'selected' : ''; ?>  ><?php echo $val; ?></option>
                                                <?php endforeach; ?>
                                                <span class='error vlError'><?php echo form_error('status'); ?></span>
                                            </select>
                                        </div>
                                    </div>
						   <div class="col-lg-8 col-md-8 col-sm-8 " style="height: 50px;">
                                <div class="form-group formWidht">
                                    <label>Banner Image  <span style="color: red">*</span></label>
                                    <input style="margin-left: 11px;" disabled title="Browse Image" name="h_image" id="" value="<?php echo ($result->image) ? $result->image : ''; ?>" class="form-control valid " readonly="" aria-invalid="false" type="text">
                                    <input name="image" value="" type="hidden">
                                    <label  class="input-group-btn crateAddImageButtonuser TopEdit">
                                        <span class="btn btn-primary">
                                            BROWSE <input name="image" id="user_pic" class="user_pic"  accept="image/gif, image/jpg, image/jpeg, image/png" type="file">
                                        </span>								
                                    </label>	
                                    <?php if (!empty($result->image)): ?>
                                        <input id='h_image' type='hidden' name='h_image'  value="<?php echo ($result->image) ? $result->image : ''; ?>" />
                                        <span class='help-inline'><img class="browseImage" src="<?php echo img_src('/assets/images/banner_image/', $result->image); ?>" width="100"></span>
                                    <?php endif; ?>
                                </div> 
                            </div>
							 <div class="col-lg-8 col-md-8 col-sm-8 " >
							
									  </div>
                            <div class="col-lg-12 col-md-12 col-sm-12  text-center" style="margin-top:80px;">
                                <input type="submit" name="save" value="SAVE" class="btn btn-primary block full-width m-b updateProductBtn">
                            </div>  
                        </div>
                    </div>
                </div>
                </form>
            </div>
        </div>
    </div>
</div>