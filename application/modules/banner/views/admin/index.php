<div id="page-wrapper" class="gray-bg dashbard-1">
    <?php breadcrumbs(array('admin/states' => 'Manage Banner')); ?>
    <div class="row border-bottom">
    </div>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12 " style="margin-top: 40px;">
                <?php print_flash_message(); ?>
                <div class="ibox float-e-margins">

                    <div class="ibox-title">
                        <h2>View Banner Images </h2>
                        <div class="ibox-tools" style="display: inline-block; float: right;">  
                            <a href="<?php echo base_url('admin/banner/create'); ?>" class="btn btn-primary block full-width m-b createuser">ADD BANNER</a>
                        </div>
                    </div>
                    <div class="table-responsive">
					<?php if ($result): ?>
                        <?php echo form_open($this->uri->uri_string(), array('class' => "form-horizontal", 'id' => 'product-listing', 'method' => 'get')); ?>
                        <div class="ibox-content borderNone">

                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <td class="text-center"><input type="checkbox"  class="i-checks check-all" name="check_all"></td>
                                        <th>Banner Image</th>   
										<th>Type</th>    
                                        <th>Status</th>
										<th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>                                    
                                    <?php foreach ($result as $val): ?>
                                            <tr>
                                                <td class="text-center">
                                                    <input type="checkbox"  class="i-checks child" name="banner_id[]" value="<?php echo $val->id; ?>">
                                                </td>
                                                <td>
													<?php if(file_exists(FCPATH.'assets/images/banner_images/'.$val->image)): ?>
													<a href="javascript:void(0);" class="image-view">
													<img class="img-rounded" alt="Cinque Terre" height="50" src="<?php echo img_src('/assets/images/banner_images/', $val->image); ?>"></a>
													<?php else: 
															   echo "Image does not exist in folder"; 
														  endif;
													?>
												</td>                                                
                                                <td><?php echo ucfirst($val->type); ?></td>
                                                <td><?php echo print_status($val->status); ?>	</td>
												<td><a href="javascript:void(0);" rel="<?php echo $val->id; ?>" data-params="<?php echo $val->image; ?>" class="delete_banner_image"><i class="fa fa-trash" aria-hidden="true"></i></a></td>
                                            </tr> 
                                            <?php
                                        endforeach; ?>
											
								</tbody> </table>
								        <button name="multi_activate" type="submit" class="viewActivateButton" value="multi_activate_action">ACTIVATE</button> 
										<button name="multi_deactivate" type="submit" class="viewDeactivateButton" value="multi_deactivate_action">DEACTIVATE</button> 
										
                           
							</form>
                        </div>
						<?php else: ?>
                                        <tr><td colspan="12">Result not found.</td></tr>
                        <?php endif;  ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-5">
                <div class="dataTables_info tableData" id="editable_info" role="status" aria-live="polite"></div>
            </div>
            
        </div>
    </div>
</div>


</div>

<!--IMAGE PREVIEW Modal -->
<div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Banner Image</h4>
            </div>
            <div class="modal-body">
                <img src="" class="img-responsive center-block">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>
<!-- end here --->