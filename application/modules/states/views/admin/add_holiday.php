<div id="page-wrapper" class="gray-bg dashbard-1">
    <!--Breadcrumbs -->	
    <?php breadcrumbs(array('admin/states/add_holiday' => 'Add State Holiday')); ?>
    <div class="row border-bottom">
    </div>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <?php print_flash_message(); ?>
            <div class="col-lg-12">
                <?php echo form_open_multipart($this->uri->uri_string(), 'class="form-horizontal"'); ?>
                <div class="ibox float-e-margins">
                    <div class="ibox-title addCatH1">
                        <h1>Add Holiday</h1>                        
                        <div class="ibox-tools">
                        </div>
                    </div>
                    <div class="ibox-content contentBorder ">
                        <div class="row contMargin">
                            <div class="col-lg-4 col-md-4 col-sm-4">
                                <div class="form-group formWidht">
                                    <label>State <span style="color: red;">*</span></label>
                                    <?php
                                    $state_id = isset($_POST['state_id']) ? $_POST['state_id'] : '';
                                    state_dropdown($state_id, 'form-control');
                                    ?> 
                                    <span class='error vlError'><?php echo form_error('state_id'); ?></span>
                                </div>                                 
                            </div>

                            <div class="col-lg-4 col-md-4 col-sm-4" >
                                <div class="form-group formWidht">
                                    <label>Holiday date*</label>
                                    <input name="date" class="form-control formWidht holiday_date" type="text" placeholder="Holiday Date">
                                    <span class='error vlError'><?php echo form_error('date'); ?></span>
                                </div>
                            </div>
                            
                            <div class="col-lg-4 col-md-4 col-sm-4" >
                                <div class="form-group formWidht">
                                    <label>Holiday name*</label>
                                    <input name="name" class="form-control formWidht" type="text" placeholder="Holiday Name">
                                    <span class='error vlError'><?php echo form_error('name'); ?></span>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-4 AddProdctInputCont">
                                <div class="form-group formWidht">
                                    <label>Description</label>
                                    <textarea class="form-control" placeholder="Holiday Description" name="description"><?php echo set_value('description', ''); ?></textarea>
                                    <span class='error vlError'><?php echo form_error('description'); ?></span>
                                </div>
                            </div>
                        </div>                        
                        <div class="ibox-content contentBorder">
                            <div class="col-lg-12 col-md-12 col-sm-12 text-right">
                                <input type="submit" class="btn btn-primary block full-width m-b updateProductBtn" name="save" value="ADD HOLIDAY"/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </form>
    </div>
</div>
</div>
