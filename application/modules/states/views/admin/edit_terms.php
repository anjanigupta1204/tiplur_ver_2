<div id="page-wrapper" class="gray-bg dashbard-1">
    <!--Breadcrumbs -->	
    <?php breadcrumbs(array('admin/states/edit_terms/'. @$this->uri->segment(4) => 'Edit State Terms & Conditions')); ?>
    <div class="row border-bottom">
    </div>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">            
            <div class="col-lg-12">
                <?php echo form_open_multipart($this->uri->uri_string(), 'class="form-horizontal"'); ?>
                <div class="ibox float-e-margins">
                    <div class="ibox-title addCatH1">
                        <h1>Edit State Terms & Conditions</h1>                        
                        <div class="ibox-tools">
                        </div>
                    </div>
                    <div class="ibox-content contentBorder ">
                        <div class="row contMargin">
                            <div class="col-lg-4 col-md-4 col-sm-4">
                                <div class="form-group formWidht">
                                    <label>State <span style="color: red;">*</span></label>
                                    <?php
                                    $state_id = isset($_POST['state_id']) ? $_POST['state_id'] : $result->state_id;
                                    state_dropdown($state_id, 'form-control');
                                    ?> 
                                    <span class='error vlError'><?php echo form_error('state_id'); ?></span>
                                </div>                                 
                            </div>

                            <div class="col-lg-4 col-md-4 col-sm-4" >
                                <div class="form-group formWidht">
                                    <label>Is Retailer<span style="color: red;">*</span></label>
                                    <select name="is_retailer" class="form-control">                                        
                                        <option value="0" <?php if ($result->is_retailer != 1) { ?>selected<?php } ?>>No</option>
                                        <option value="1" <?php if ($result->is_retailer == 1) { ?>selected<?php } ?>>Yes</option>
                                    </select>
                                    <span class='error vlError'><?php echo form_error('is_retailer'); ?></span>
                                </div>
                            </div>

                            <div class="col-lg-4 col-md-4 col-sm-4">
                                <div class="form-group formWidht">
                                    <label>Terms & Conditions<span style="color: red;">*</span></label>
                                    <textarea class="form-control" id="editor1" placeholder="Terms & Conditions" name="content"><?php echo set_value('content', $result->content); ?></textarea>
                                    <span class='error vlError'><?php echo form_error('content'); ?></span>
                                </div>
                            </div>
                        </div>                        
                        <div class="ibox-content contentBorder">
                            <div class="col-lg-12 col-md-12 col-sm-12 text-right">
                                <input type="submit" class="btn btn-primary block full-width m-b updateProductBtn" name="save" value="EDIT T&C"/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </form>
    </div>
</div>
</div>
