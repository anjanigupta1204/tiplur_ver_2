<div id="page-wrapper" class="gray-bg dashbard-1">
    <!--Breadcrumbs -->	
    <?php breadcrumbs(array('admin/states/product-limit-create' => 'Add Products Limit')); ?>
    <div class="row border-bottom">
    </div>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">            
            <div class="col-lg-12">                
                <?php echo form_open_multipart($this->uri->uri_string(), 'class="form-horizontal"'); ?>
                <div class="ibox float-e-margins">
                    <div class="ibox-title addCatH1">
                        <h1>Add Product Limit</h1>                        
                        <div class="ibox-tools">
                        </div>
                    </div>
                    <div class="ibox-content contentBorder ">
                        <div class="row contMargin">
                            <div class="col-lg-3 col-md-3 col-sm-3">
                                <div class="form-group formWidht">
                                    <label>State Name <span style="color: red;">*</span></label>
                                    <?php
										$state_id = isset($_POST['state_id']) ? $_POST['state_id'] : '';
										state_dropdown($state_id, 'form-control');
                                    ?> 
                                    <span class='error vlError'><?php echo form_error('state_id'); ?></span>
                                </div>                                 
                            </div>
                        </div>
						<?php $brand_array = array('1'=>'DOMESTIC','2'=>'IMPORTED','3'=>'WINE','4'=>'BEER'); 
						      foreach($brand_array as $k=>$brand):
						?>
                        <div class="row contMargin">
                            <div class="col-lg-2 col-md-2 col-sm-2">
                                <div class="form-group formWidht">
                                    <label style="">&nbsp;</label>
                                    <input class="form-control formWidht" type="text"  value="<?php echo $brand; ?>" name="brandname<?php echo $k; ?>" readonly>
                                </div>                                 
                            </div>
                            
                            <div class="col-lg-3 col-md-3 col-sm-3">
                                <div class="form-group formWidht">
                                    <label>Quantity<span style="color: red;">*</span></label>
                                    <input class="form-control formWidht" type="text" placeholder="Number Of Bottles" value="<?php echo set_value('quantity'.$k, ''); ?>" name="quantity<?php echo $k; ?>">
                                    <span class='error vlError'><?php echo form_error('quantity'.$k); ?></span>
                                </div>                                 
                            </div>

                            <div class="col-lg-3 col-md-3 col-sm-3">
                                <div class="form-group formWidht">
                                    <label>Weight (ml)<span style="color: red;">*</span> </label>
                                    <input type="text" placeholder="Weight in ml" value="<?php echo set_value('weight'.$k, ''); ?>" name="weight<?php echo $k; ?>"  class="form-control formWidht ">
                                    <span class='error vlError'><?php echo form_error('weight'.$k); ?></span>
                                </div>
                            </div>
                        </div> 
                       <?php endforeach; ?>						
                        <div class="ibox-content contentBorder">
                            <div class="col-lg-12 col-md-12 col-sm-12 text-right">
                                <input type="submit" class="btn btn-primary block full-width m-b updateProductBtn" name="save" value="ADD LIMIT"/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </form>
    </div>
</div>
</div>
