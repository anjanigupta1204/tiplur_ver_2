<div id="page-wrapper" class="gray-bg dashbard-1">
    <!--Breadcrumbs -->	
    <?php breadcrumbs(array('admin/states/create' => 'Add State')); ?>
    <div class="row border-bottom">
    </div>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">            
            <div class="col-lg-12">                
                <?php echo form_open_multipart($this->uri->uri_string(), 'class="form-horizontal"'); ?>
                <div class="ibox float-e-margins">
                    <div class="ibox-title addCatH1">
                        <h1>Add State</h1>                        
                        <div class="ibox-tools">
                        </div>
                    </div>
                    <div class="ibox-content contentBorder ">
                        <div class="row contMargin">
                            
                            <div class="col-lg-3 col-md-3 col-sm-3">
                                <div class="form-group formWidht">
                                    <label>State Name <span style="color: red;">*</span></label>
                                    <input class="form-control formWidht" type="text" placeholder="Name" value="<?php echo set_value('name', ''); ?>" name="name">
                                    <span class='error vlError'><?php echo form_error('name'); ?></span>
                                </div>                                 
                            </div>
                            
                            <div class="col-lg-2 col-md-2 col-sm-2">
                                <div class="form-group formWidht">
                                    <label>Allowed Age Limit<span style="color: red;">*</span></label>
                                    <input class="form-control formWidht" type="text" placeholder="Age Limit" value="<?php echo set_value('age', ''); ?>" name="age">
                                    <span class='error vlError'><?php echo form_error('age'); ?></span>
                                </div>                                 
                            </div>

                            <div class="col-lg-2 col-md-2 col-sm-2">
                                <div class="form-group formWidht">
                                    <label>Opening Time<span style="color: red;">*</span> </label>
                                    <input type="text" placeholder="Opening Time" value="<?php echo set_value('opening_time', ''); ?>" name="opening_time" id="opening_time" class="form-control formWidht opening_time">
                                    <span class='error vlError'><?php echo form_error('opening_time'); ?></span>
                                </div>
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-2">
                                <div class="form-group formWidht">
                                    <label>Closing Time<span style="color: red;">*</span></label>
                                    <input type="text" placeholder="Closing Time" name="closing_time" id="closing_time" value="<?php echo set_value('closing_time', ''); ?>" class="form-control formWidht closing_time">
                                    <span class='error vlError'><?php echo form_error('closing_time'); ?></span>
                                </div>
                            </div>

                            <div class="col-lg-3 col-md-3 col-sm-3">
                                <div class="form-group formWidht">
                                    <label>Status<span style="color: red;">*</span></label>
                                    <select name="status" class="form-control">
                                        <?php
                                        $status = status();
                                        foreach ($status as $k => $val):
                                            ?>
                                            <option value="<?php echo $k; ?>" <?php echo ($k == @$_POST['status']) ? 'selected' : ''; ?>  ><?php echo $val; ?></option>
                                        <?php endforeach; ?>                                            
                                    </select>
                                    <span class='error vlError'><?php echo form_error('status'); ?></span>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-3">
                                <div class="form-group formWidht">
                                    <label>Is available?<span style="color: red;">*</span></label>
                                    <select name="is_available" class="form-control">
                                    
                                        <?php
                                       
                                        $is_available= is_available();
                                        foreach ($is_available as $k => $val):
                                            ?>
                                            <option value="<?php echo $k; ?>" <?php echo ($k == @$_POST['is_available']) ? 'selected' : ''; ?>  ><?php echo $val; ?></option>
                                        <?php endforeach; ?>                                            
                                    </select>
                                    <span class='error vlError'><?php echo form_error('is_available'); ?></span>
                                </div>
                            </div>
                        </div>                        
                        <div class="ibox-content contentBorder">
                            <div class="col-lg-12 col-md-12 col-sm-12 text-right">
                                <input type="submit" class="btn btn-primary block full-width m-b updateProductBtn" name="save" value="ADD STATE"/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </form>
    </div>
</div>
</div>
