<?php //if($this->session->userdata('auth_otp')):   ?>
<div class="middle-box text-center loginscreen animated fadeInDown" style="">
	<div>
		<div>                
			<div class="loginLogo"></div>
		</div> 
		<?php print_flash_message(); ?>
		<?php //echo $test; ?>
		<?php echo form_open($this->uri->uri_string(), array('class' => 'm-t loginLlbl', 'autocomplete' => 'off')); ?>
			<div class="signin_message text-center">
				<span class='error'><?php echo form_error('email_pass_match'); ?></span>
			</div>
			<div class="form-group">   
				<label for="usr">Username</label>
				<input type="email" name="email" class="form-control" value="<?php echo set_value('email', ''); ?>" placeholder="Username" required="">
				<span class='error'><?php echo form_error('email'); ?></span>
			</div>
			<div class="form-group">
				<label for="pwd">Password</label>
				<input type="password" name="password" class="form-control" placeholder="Password" required="">
				<span class='error'><?php echo form_error('password'); ?></span>
			</div>
			<?php /*<div class="form-group">
				<label for="pwd">OTP</label>
				<input type="text" name="otp" class="form-control" placeholder="Enter Your OTP" required="">
				<span class='error'><?php echo form_error('otp'); ?></span>
			</div> */ ?>
			<div class="forgetDiv">
				<a href="javascript:void(0)" id="FpModal"><small>Forgot password?</small></a>
			</div>
			<input type="submit" class="btn btn-primary block full-width m-b loginBtn" name="login" value="LOG IN" required=""> 
			
		
		<?php echo form_close(); ?>       
	</div>
</div>
<?php /* else: ?>

<div class="middle-box text-center loginscreen animated fadeInDown">
	<div>
		<div>                
			<div class="loginLogo"></div>
		</div> 
		<?php print_flash_message(); ?>
		<?php //echo $test; ?>
		<?php echo form_open($this->uri->uri_string(), array('class' => 'm-t loginLlbl', 'autocomplete' => 'off')); ?>
			<div class="signin_message text-center">
				<span class='error'><?php echo form_error('email_pass_match'); ?></span>
			</div>
			<div class="form-group">   
				<label for="usr">Mobile</label>
				<input type="text" name="mobile" class="form-control" value="<?php echo set_value('mobile', ''); ?>" placeholder="Enter Your Mobile Number" required="">
				<span class='error'><?php echo form_error('mobile'); ?></span>
			</div>
			
			
			<input type="submit" class="btn btn-primary block full-width m-b loginBtn" name="otp-login" value="Send" required=""> 
			
		
		<?php echo form_close(); ?>       
	</div>
</div>
<?php endif; */ ?>

<!--Forgot Password Modal -->
		  <div class="modal fade" id="Forgot-Pass" role="dialog">
			<div class="modal-dialog modal-md">
			  <div class="modal-content">
				<div class="modal-body" style="padding:30px;">
				 <button type="button" class="close" data-dismiss="modal">&times;</button>
				 <form id="forgot_pass_form" name="forgot_pass_form"  method="post">
				 <h3 style="text-align:center; font-weight: bold; font-size:22px">Forgot Password</h3><br>
				 <div id="fp_msg" style="color:green;"></div>
                 <div id="fp_msg_error" style="color:red;"></div>
				 <input type="text" id="email" name="email"   class="required form-control" placeholder="Enter Email:" aria-required="true"><br>
				  <button type="submit" class="btn btn-primary"  style="text-align: center;">Submit</button>
				  </form>
				</div>   
			  </div>
			</div>
		  </div>