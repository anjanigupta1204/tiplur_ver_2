<div id="page-wrapper" class="gray-bg dashbard-1">
    <div class="row wrapper border-bottom white-bg page-heading bradPading">
        <div class="col-lg-8">
            <!--ol class="breadcrumb">
                <li>
                    <a href="<?php base_url(); ?>dashboard">Home</a>
                </li>
                
            </ol-->
        </div>
        <div class="col-lg-4 text-right"><i class="fa fa-calendar" aria-hidden="true"></i><span class="dateMargin"><?php echo date('d F Y', strtotime(date('Y-m-d H:i:s'))); ?></span> </div>
    </div>
    <div class="row border-bottom">
    </div>
    <div class="row  border-bottom  dashboard-header">
        <div class="wrapper wrapper-content animated fadeInRight" style="padding-bottom: 0px;">
            <div class="row">
                <h2>Overview<small class="subHeadStyle"></small></h2>
                <div class="col-lg-3">
                    <a href="">
                        <div class="widget style1 firstWidget">
                            <div class="row">
                                <div class="col-xs-4 text-center">
                                    <div class="customersImg"></div>
                                </div>
                                <div class="col-xs-8 textColor">
                                    <h2 class="font-bold"><?php echo ($retailers_count) ? $retailers_count : '0'; ?></h2>
                                    <span> RETAILERS </span>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-lg-3">
                    <a href="">
                        <div class="widget style1 SecondtWidget">
                            <div class="row">
                                <div class="col-xs-4 text-center">
                                    <div class="orderImg"></div>
                                </div>
                                <div class="col-xs-8 textColor">
                                    <h2 class="font-bold"><?php echo ($total_order_count) ? $total_order_count : '0'; ?></h2>
                                    <span> TOTAL ORDERS </span>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-lg-3">
                    <a>
                        <div class="widget style1 thirdWidget">
                            <div class="row">
                                <div class="col-xs-4 text-center">
                                    <div class="orderImg"></div>
                                </div>
                                <div class="col-xs-8 textColor">
                                    <h2 class="font-bold"><?php echo ($recent_order_count) ? $recent_order_count : '0'; ?></h2>
                                    <span> RECENT ORDERS </span>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-lg-3">
                    <a href="">
                        <div class="widget style1 fourthWidget">
                            <div class="row">
                                <div class="col-xs-4 text-center">
                                    <div class="customersImg"></div>
                                </div>
                                <div class="col-xs-8 textColor">
                                    <h2 class="font-bold"><?php echo ($customers_count) ? $customers_count : '0'; ?></h2>
                                    <span> CUSTOMERS </span>
                                </div>
                            </div>
                        </div>
                </div>
                </a>
            </div>
        </div>
    </div>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-6">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Recent Orders</h5>
                    </div>
                    <div class="ibox-content">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>Order No</th>
                                    <th>Customer</th>
                                    <th>Amount</th>
                                    <th>Date & Time</th>
                                    <th>Status</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                if ($recent_orders):
								//print_r($recent_orders);die;
                                    foreach ($recent_orders as $recent):
                                        ?>
                                        <tr>
                                            <td><a href="<?php echo base_url('admin/orders/view_order?id=').$recent->order_id;?>"><?php echo $recent->order_id;?></a></td>
                                            <td><?php echo $recent->user;?></td>
                                            <td><?php echo intval($recent->total_amount);?></td>
                                            <td><?php echo site_date_time($recent->order_date); ?></td>
                                            <td><?php echo print_order_status($recent->status); ?></td>
                                        </tr>
                                        <?php
                                    endforeach;
                                endif;
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Pending Orders</h5>
                    </div>
                    <div class="ibox-content">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>Order No</th>
                                    <th>Customer</th>
                                    <th>Amount</th>
                                    <th>Date & Time</th>
                                    <th>Status</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                if ($process_order_list):
                                    foreach ($process_order_list as $process):
                                        ?>
                                        <tr>
                                            <td><a href="<?php echo base_url('admin/orders/view_order?id=').$process->order_id;?>"><?php echo $process->order_id;?></a></td>
                                            <td><?php echo $process->user;?></td>
                                            <td><?php echo intval($process->total_amount);?></td>
                                            <td><?php echo site_date_time($process->order_date); ?></td>
                                            <td><?php echo print_order_status($process->status); ?></td>
                                        </tr>
                                        <?php
                                    endforeach;
                                endif;
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


