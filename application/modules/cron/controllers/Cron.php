<?php

class Cron extends MX_Controller{ 

    function __construct() {
    	error_reporting ( 0 );
    	$this->load->model ( 'api/Api_service' );
    	$this->load->model ( 'api/Api_dao' );
    	include_once './application/objects/Response.php';
    	$this->load->helper ( 'string' );
		$this->load->library ( array (
    			'form_validation'
    	) );
    } 
	

    
    public function auto_reject_orders(){
    	
    	$apiService = new Api_service();
    	
    	$response = $apiService->auto_reject_orders();
    	$data =  array (
    			'status' => 1,
    			'message' => "Auto rejected orders",
    			'jsonData' => $response
    	);
    	echo json_encode($data);
    }
	
	public function cartNotificationAlert()
	{
		$result = $this->db->select('cart.user_id,cart.abandoned_cart_flag,cart.retailer_id,users.mobile,d1.deviceId,d1.deviceType,d1.fcmId,d2.is_logged_in')
		->join('users','users.id=cart.user_id','left')
		->join('device_info d1','d1.user_id=cart.user_id','left')
		->join('device_info d2','d2.user_id=cart.retailer_id','left')
		->where(array('cart.abandoned_cart_flag'=>'0','d2.is_logged_in'=>'1'))
		->group_by('cart.user_id')->get('cart')->result_array();
		
		if(!empty($result))
		{
			$msg = array('title' => 'Abandoned Cart', 'message' => 'You have products on your cart, you may submit to chosen retailer or clear the cart', 'type' => '9', 'order_id' => '', 'notification_id' => '', 'notified_to' =>'', 'reason'=>'Abandoned Cart');
			
			/* android and ios test ids */
			
			//$result1 = array('561b75188d072d9ee2e1047200d04b958aaf3a72905b7c2b2e43c853131c6e81','a5d8ec9c95da9452e1208cb1a99d11e4f89f84214caefd702789ac792ad182f4');
			
			///$androidIds = array('dB-AqzpweMg:APA91bFSHDq2RSz30IzxiukBx86Gl7IxL-OBDib0Z_44vDAnLEIYYKPdZ0cib8fRC3ioM0LFoNnc9ZRydhZABmh9f9O9YIRENc9jybwfxHmjB6ox4xIncpcv1PDZ4JhYGff-c7FA1YiN','cP19v3lptkE:APA91bGDW8o-1SpyNMOUc6_WpOZXqkPgd7UWfKpaWIzd2Ses3Tr9g5z8yvlOVCOalOQPTnEHOpzGE7sXbaaC6mgUDp-p-aY5ltAk_wS357SGV-ukjsXFBMZDM5Cosw9sb59vQF9PAOG_');
			
			/* end here */
			
			foreach($result as $res)
			{  
                
				//ios notification
				if($res['deviceType'] == 'iOS'):
					$this->push_notification->send_ios_notification($res['fcmId'],$msg);
				elseif($res['deviceType'] == 'android'):
			    //Android notification	
                    $androidIds[] = $res['fcmId'];				
				endif;
				
				
				$cart_data['abandoned_cart_flag']='1';
				$update_cart = $this->db->where('user_id',$res['user_id'])->update('cart',$cart_data);
			} 
			
			if(!empty($androidIds))
			{
				$fields = array('registration_ids' => $androidIds, 'data' => $msg);
				$this->push_notification->send_android_notification($fields);
			}
			
		}
	}
	
	public function cartSmsAlert()
	{
		$result = $this->db->select('cart.user_id,cart.abandoned_cart_flag,users.mobile')
		->join('users','users.id=cart.user_id','left')
		->where(array('cart.abandoned_cart_flag'=>'1'))
		->group_by('cart.user_id')->get('cart')->result_array();
		
		if(!empty($result))
		{
			$mobileNumbers = array_column($result,'mobile');
			
			//$mobileNumbers = array('9873053504','8743892991');  
            $userIds = array_column($result,'user_id');	
			$msg = 'You have products on your cart, you may submit to chosen retailer or clear the cart';
			$mh = curl_multi_init();
			$handles = array();
			$sender_id = 'TIPLUR';
		    $user_id = urlencode('rakshat.chopra@gmail.com');
		    $pwd     = urlencode('RC@123tm');
		    $message = urlencode($msg);
			
			for($i = 0 ; $i < count($mobileNumbers) ; $i++)
			{
				$URL ="http://49.50.77.216/API/SMSHttp.aspx?UserId=".$user_id."&pwd=".$pwd."&Message=".$message."&Contacts=".$mobileNumbers[$i]."&SenderId=".$sender_id."";
				$ch = curl_init();
				$handles[] = $ch;

				curl_setopt($ch, CURLOPT_URL, $URL);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
				curl_multi_add_handle($mh,$ch);
			} 

			$running = null;
			do 
			{
			   curl_multi_exec($mh, $running);
			} while ($running);

			foreach($handles as $ch)
			{
			   $result = curl_multi_getcontent($ch);

			   $dd = json_decode($result,true);
			   $final_data[] = $dd;
				
				curl_multi_remove_handle($mh, $ch);
				curl_close($ch);
			}
			
			//update flag value 2 in cart table 
			$update['abandoned_cart_flag'] = '2';
			$this->db->where_in('user_id',$userIds)->update('cart',$update);
			
		}
		
	}
	
    
	
	
	
}
