<div id="page-wrapper" class="gray-bg dashbard-1">
    <!--Breadcrumbs -->	
    <?php breadcrumbs(array('admin/notification/send_notifications' => 'Send Notification')); ?>
    <div class="row border-bottom">
    </div>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <?php print_flash_message(); ?>
            <div class="col-lg-12">
                <?php echo form_open_multipart($this->uri->uri_string(), 'class="form-horizontal"'); ?>
                <div class="ibox float-e-margins">
                    <div class="ibox-title addCatH1">
                        <h1>Send Notification</h1>
                        <div class="ibox-tools"></div>
                    </div>
                    <div class="ibox-content contentBorder ">
                        <div class="row contMargin">

                            <div class="col-lg-12 col-md-12 col-sm-12 AddProdctInputCont">
                                <div class="form-group formWidht">
                                    <label>Type <span>*</span></label>
                                    <select name="type" class="form-control m-b addContDrop m-b-0">
                                        <option value="">Please select</option>
                                        <option value="all">All</option>
                                        <option value="user">Users</option>
                                        <option value="retailer">Retailers</option>
                                    </select>
                                    <span class='error vlError'><?php echo form_error('type'); ?></span>
                                </div>
                            </div>

                            <div class="col-lg-12 col-md-12 col-sm-12 AddProdctInputCont">
                                <div class="form-group formWidht">
                                    <label>Title <span style="color: red;">*</span></label>
                                    <input type="text" placeholder="" name="title" value="<?php echo set_value('title', ''); ?>" id="exampleInputEmail2" class="form-control formWidht">
                                </div>
                                <span class='error vlError'><?php echo form_error('title'); ?></span>
                            </div>

                            <div class="col-lg-12 col-md-12 col-sm-12 AddProdctInputCont category-textarea">
                                <div class="form-group formWidht">
                                    <label>Message <span style="color: red;">*</span></label>
                                    <textarea class="form-control" name="message" style="width: 100%; height: 150px;"><?php echo set_value('message', ''); ?></textarea>
                                    <span class='error vlError'><?php echo form_error('message'); ?></span>
                                </div>
                            </div> 

                            <div class="col-lg-6 col-md-6 col-sm-6 AddProdctInputCont">
                                <div class="form-group formWidht">
                                    <label>Image </label>
                                    <input type="file" class="form-control image-validate" name="image" id="image" />
                                    <span class='error vlError'></span>
                                </div>
                            </div>                            
                        </div>

                        <div class="ibox-content contentBorder">
                            <div class="col-lg-12 col-md-12 col-sm-12 text-right">
                                <input type="submit" class="btn btn-primary block full-width m-b updateProductBtn" name="save" value="Send"/>
                            </div>
                        </div>

                    </div></div>
            </div>
        </div>
        </form>
    </div>
</div>
</div>
