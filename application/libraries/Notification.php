<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Notification {
	
   private $ci; 
   public function __construct() {
       $this->ci = & get_instance();
   }


   public function send_notification($data){

		//FCM ids of user
   		$fcm_ids_array = $this->get_fcm_ids($data['notified_to']);
		
		//FCM credentials
		$fcmApiKey = FCM_API_KEY;
		$url = FCM_API_URL;
		
		$registrationIds = $fcm_ids_array;
		
		//save notification
		$notification_data = array();
		$notification_data['order_number'] = $data['order_id']; 
		$notification_data['notified_to'] = $data['notified_to']; 
		$notification_data['requested_by'] = $data['requested_by']; 
		$notification_data['type'] = $data['type'];
		$notification_data['title'] = $data['title']; 
		$notification_data['message'] = $data['message']; 
		$notification_data['creation_date'] = date('Y-m-d H:i:s');
		
		$notification_id = $this->save_notification($notification_data);
		
		// prepare the message
		$msg = array('title' => $data['title'], 'message' => $data['message'], 'type' => $data['type'], 'order_id' => $data['order_id'], 'notification_id' => $notification_id, 'notified_to' =>$data['notified_to'], 'reason'=>$data['reason']);
		$fields = array('registration_ids' => $registrationIds, 'data' => $msg);
		
		$headers = array(
				'Authorization: key=' . $fcmApiKey,
				'Content-Type: application/json'
		);
		
		$ch = curl_init();
		curl_setopt( $ch,CURLOPT_URL, $url );
		curl_setopt( $ch,CURLOPT_POST, true );
		curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
		curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
		curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
		curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
		$result = curl_exec( $ch );
		
		// Execute post
		
		if ($result === FALSE) {
			die('Curl failed: ' . curl_error($ch));
		}
		// Close connection
		curl_close($ch);
		
		return json_decode($result);
	}
	
	private function get_fcm_ids($user_id){
		$query = $this->ci->db->select('fcmId')->where('user_id', $user_id)->get('device_info')->result();
		$ids = array();
		foreach ($query as $row){
			array_push($ids, $row->fcmId);
		}
		return $ids;
	}
	
	private function save_notification($data){
		$query = $this->ci->db->insert('notification', $data);
		return $this->ci->db->insert_id();
	}
	
	
   
   
}
