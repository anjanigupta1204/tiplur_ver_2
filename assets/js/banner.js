jQuery(document).on("change","#user_pic", function() 
{
    $(".file-error").html("");
	$(".InputBox").css("border-color","#F0F0F0");
	var file_size = $('#user_pic')[0].files[0].size;
	
	var fileExtension = ['jpeg', 'png', 'jpg']; 
	
	if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) 
	{
	    $(".file-error").html("Only formats are allowed : " + fileExtension.join(', '));
		$('#old_user_pic').val('');
		return false;
	}else if(file_size>2097152) 
	{
		$(".file-error").html("File size is greater than 2MB");
		$(".InputBox").css("border-color","#FF0000");
		$('#old_user_pic').val('');
		return false;
	}else{ 
	 return true;
	}
});


// question image view //
$('.image-view').on('click', function () {

    var img = $(this).children('img').attr('src');
	$("#myModal .modal-body").children('img').attr('src',img);
    $("#myModal").modal('show');
});

$(document).on('click','.delete_banner_image',function(){ 
    if (confirm('Are you sure you want to delete this file ?')) {
		$file_id   = $(this).attr('rel'); 
        $file_name = $(this).attr('data-params'); 		
        $.ajax({
			type: "POST",
			url: base_url + 'admin/banner/delete_banner_img/',
			data: {file_id:$file_id,file_name:$file_name},
			success: function (data) {
				    if(data == '1')
					{
						location.reload();
					}
			}
		});
    }
});