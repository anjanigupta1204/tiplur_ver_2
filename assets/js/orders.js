$('.datepicker').datetimepicker({
    format: 'YYYY-MM-DD'
});

$('.order-decline').click(function (e) {
    e.preventDefault();
    var id = $(this).attr('data-id');
    $('#order_id').val(id);
    $("#declineOrderModal").modal({backdrop: 'static',keyboard: false});
});