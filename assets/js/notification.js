$(document).ready(function() {       
    $('.image-validate').bind('change', function () 
    { 
        var ext = $('.image-validate').val().split('.').pop().toLowerCase();
        if ($.inArray(ext, ['gif','png','jpg','jpeg']) == -1)
        {
            alert("This file is not allowed, please upload valid file.");
            $(this).val('');
        }else if(this.files[0].size > 1*1024*1024)
        {
            alert("This file size should not be greater than 1 MB.");
            $(this).val('');
        }else{
            return true;
        }
    });
});


