$(function ()
{
    $('.opening_time').datetimepicker({
        format: 'LT'
    });

    $('.closing_time').datetimepicker(
            {
                format: 'LT'
            });

    $(".opening_time").on("dp.change", function (e)
    {
        $('.closing_time').data("DateTimePicker").minDate(e.date);
    });
});

/* product multi images upload */
$('#upload_album').change(function ($file) {
    $this = this;
    $loder_li_html = '<li id="file_loder"><div class="single-photo"><img class="img-responsive" src="' + base_url + 'assets/uploads/products/doc_loder.gif"></div></li>';
    $($this).closest('li').before($loder_li_html);


    $check_uploded = $("input[name='is_uploaded']").val();


    data = new FormData();
    data.append('check_uploded', $check_uploded);


    $total_files = $file.target.files.length;
    for (var x = 0; x < $total_files; x++) {
        data.append(x, $file.target.files[x]);
    }


    $.ajax({
        type: "POST",
        url: base_url + 'admin/states/upload_doc',
        data: data,
        cache: false,
        processData: false,
        contentType: false,
        success: function ($res) {
            $resl = jQuery.parseJSON($res);
            $files_uploaded = $resl.file;
            $file_name = $resl.file_name;
            $errors = $resl.error;

            //update files image
            if ($files_uploaded.length > 0) {
                for (var x = 0; x < $files_uploaded.length; x++) {
                    var path = $resl.file_path[x];
                    $doc_li_html = '<li><div class="single-photo"><img class="img-responsive" src="' + $files_uploaded[x] + '"></div><div class="photos-option"><div class="text-right"><a title="Delete" class="btn delete_event_file" href="javascript:void(0);" rel="temp@_@' + $file_name[x] + '">X</a></div></div></li>';
                    $($this).closest('li').before($doc_li_html);
                }
            }

            //update errors
            if ($errors.length > 0) {
                for (var x = 0; x < $errors.length; x++) {
                    alert($errors[x]);
                }
            }

            //if images uploded then change status
            if (($check_uploded == 'no') && ($files_uploaded.length > 0)) {
                $("input[name='is_uploaded']").val('yes');
            }


            $('#file_loder').remove();
        }
    });

});



/* pro image delete */
$(document).on('click', '.delete_event_file', function () {
    if (confirm('This item will be permanently deleted and cannot be recovered. Are you sure?')) {
        $(this).closest('li').remove();
        $file_id = $(this).attr('rel');
        $.post(base_url + "admin/states/delete_pro_img/", {file_id: $file_id});
    }
});


//js for prescription image preview
jQuery(document).on('click', '.img_pop', function ()
{
    $id = $(this).attr('alt');
    var datastring = 'id=' + $id;
    if ($id)
    {
        $.ajax({
            type: "POST",
            url: base_url + 'admin/states/image_slider_data',
            data: datastring,
            success: function (result)
            {
                $('#show_img_slider').html(result);
            }
        });
    }
    $('#imagemodal').modal('show');
});

//state holidays field add

$(document).on('click', '.add_holiday', function () {


    $html = $('#holidays').html();
    $('#append_holidays').append($html);
    $('.hdate').datetimepicker({
        format: 'YYYY-MM-DD',
    });
});


/* state holidays fields remove*/
$(document).on('click', '.remove_holiday', function () {
    $(this).parent('div').parent('.rm').remove();
});

$(function () {
    $('.holiday_date').datetimepicker({
        format: 'YYYY-MM-DD',
    });
});

$(document).ready(function(){
	CKEDITOR.replace('editor1',{
     height: 100
});
});